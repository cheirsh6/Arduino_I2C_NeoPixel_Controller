/*-------------------------------------------------------------------------*\
|  Arduino I2C NeoPixel Driver                                              |
|                                                                           |
|  Drive up to 85 NeoPixel LEDs from I2C.  Uses register addressing scheme  |
|                                                                           |
|  Adam Honse <calcprogrammer1@gmail.com>                 3/3/2022          |
\*-------------------------------------------------------------------------*/

#include <Adafruit_NeoPixel.h>
#include <Wire.h>

/*-------------------------------------------------------------------------*\
| NeoPixel (ARGB) output on Pin 2                                           |
\*-------------------------------------------------------------------------*/
#define PIN            2

/*-------------------------------------------------------------------------*\
| 85 pixels maximum for 8-bit i2c address space                             |
\*-------------------------------------------------------------------------*/
#define NUMPIXELS      85

/*-------------------------------------------------------------------------*\
| Index tracks current position in color buffer                             |
\*-------------------------------------------------------------------------*/
volatile unsigned char index = 0;
volatile unsigned char color_buf[NUMPIXELS * 3];
volatile bool refresh = false;

/*-------------------------------------------------------------------------*\
| Use GRB 800KHz format for WS2812B and compatible strips                   |
\*-------------------------------------------------------------------------*/
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

/*-------------------------------------------------------------------------*\
| Setup                                                                     |
|                                                                           |
|   Start i2c on address 0x08 and start pixels on pin 2                     |
\*-------------------------------------------------------------------------*/
void setup()
{
  memset(color_buf, 0x00, sizeof(color_buf));
  refresh = true;
  
  Wire.begin(0x08);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);

  pixels.begin();
}

/*-------------------------------------------------------------------------*\
| Loop                                                                      |
|                                                                           |
|   Update the pixels from the color buffer                                 |
\*-------------------------------------------------------------------------*/
void loop()
{
  if(refresh)
  {
    for(char i = 0; i < NUMPIXELS; i++)
    {
      pixels.setPixelColor(i, pixels.Color(color_buf[i * 3], color_buf[i * 3 + 1], color_buf[i * 3 + 2]));
    }
    pixels.show();
  }

  refresh = false;

  /*-----------------------------------------------------------------------*\
  | This shouldn't be necessary, but it prevents lockups                    |
  \*-----------------------------------------------------------------------*/
  Wire.begin(0x08);
}

/*-------------------------------------------------------------------------*\
| Receive Event                                                             |
|                                                                           |
|   First received byte is the start index, additional bytes are written to |
|   color buffer                                                            |
\*-------------------------------------------------------------------------*/
void receiveEvent(int)
{
  index = Wire.read();

  if(index == 0xFF)
  {
    refresh = true;
  }
  
  while(Wire.available())
  {
    unsigned char read_val = Wire.read();
    
    if(index < NUMPIXELS * 3)
    {
      color_buf[index] = read_val;
      index++;
    }
  }
}

/*-------------------------------------------------------------------------*\
| Request Event                                                             |
|                                                                           |
|   Write bytes from the color buffer and increment the index after each    |
|   read                                                                    |
\*-------------------------------------------------------------------------*/
void requestEvent()
{
  if(index < NUMPIXELS * 3)
  {
    Wire.write(color_buf[index]);
    index++;
  }
  else
  {
    Wire.write(0);
  }
}
